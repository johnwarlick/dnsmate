from flask import Flask, redirect, render_template
from dotenv import load_dotenv
from flask_wtf import FlaskForm
from wtforms import StringField
from wtforms.validators import DataRequired
from fqdn import FQDN
import os

load_dotenv()

class LookupForm(FlaskForm):
    hostname = StringField('hostname', validators=[DataRequired()])

app = Flask(__name__)

# TODO: come up with way to handle secret keys on production
app.secret_key = os.getenv('SECRET_KEY')

@app.route("/")
def home():
    form = LookupForm()
    return render_template("index.html", form=form)

@app.route("/lookup", methods=['POST'])
def lookup():
    form = LookupForm()
    if form.validate_on_submit():
        hostname = form.hostname.data
        fqdn = FQDN(hostname)
        if fqdn.is_valid: 
            return redirect('/results/'+hostname) 
    return redirect('/')

@app.route('/results/<hostname>', methods=['GET'])
def results(hostname):
    return render_template("results.html", hostname=hostname)

if __name__ == "__main__":
    app.run(host='0.0.0.0')
